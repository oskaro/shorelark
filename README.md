# Shorelark

This is a project I did, following the [Learning to Fly: Let's simulate evolution in Rust!](https://pwy.io/posts/learning-to-fly-pt1/).

I checked in this project a lot later than I actually created it, so I am not sure if any of it is created by me.
So I will give all credit to [Patryk Wychowaniec](https://github.com/Patryk27)! Thanks for creating a great project to follow along.
